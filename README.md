# HOW TO USE THIS REPOSITORY

## contributing

- fork/branch
- vs code
- sync (make sure in sync)
- terminal
- git branch -l
- git switch <branchname>
- make changes 
- test and verify
- if local, run mkdocs (see below)
- commit
- MR


## mkdocs

To locally test `mkdocs` you need to run `pip install -e .` in the root directory of the project, so it can load the custom `setup.py` to manage the code blocks in the guide.

`mkdocs serve`