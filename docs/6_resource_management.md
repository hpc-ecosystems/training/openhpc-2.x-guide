# 6 Resource Management

In this chapter we will configure and start up the *Slurm Workload Manager* ("*Slurm*"). This must be performed on both the System Management Server host (**smshost**) and all compute nodes. *Slurm* uses the *MUNGE* authentication service to manage credentials. As such, the *MUNGE* daemon must be running on both the master host and the compute nodes within the resource management pool.

!!! quote "Note"

    Anticipated time to complete this chapter: TBC from user feedback. 

## 6.1. Slurm Configuration
----

1. To start the *Slurm* configuration we will make use of the ohpc-provided file.
    ```bash prefix="[root@smshost vagrant]#"
    sudo cp /etc/slurm/slurm.conf.ohpc /etc/slurm/slurm.conf
    sudo cp /etc/slurm/cgroup.conf.example /etc/slurm/cgroup.conf
    ```

    ??? question "Click here to learn more about what the above commands are doing."
        We are copying the preconfigured OpenHPC files for *Slurm* into the production *Slurm* folders on the **smshost** located at `/etc/slurm/`.  

1. The default configuration that is provided for *Slurm* will not be correct for this lab. View the contents of the default `slurm.conf` file.

    ```bash prefix="[root@smshost vagrant]#"
    cat /etc/slurm/slurm.conf | grep SlurmctldHost
    ```

    ```{: .shell .optional-language-as-class .no-copy .prevent-select}
    OUTPUT:

    # SlurmctldHost=linux0 
    ```
    
1. Once content of the default `slurm.conf` file is known, you can modify it to match the environment for the lab. We will replace the default name for the `SlurmctldHost` with the `$sms_name`.

    ```bash prefix="[root@smshost vagrant]#"
    sudo perl -pi -e "s/SlurmctldHost=\S+/SlurmctldHost=${sms_name}/" /etc/slurm/slurm.conf
    cat /etc/slurm/slurm.conf | grep SlurmctldHost
    ```

    ```{: .shell .optional-language-as-class .no-copy .prevent-select}
    OUTPUT:

    # SlurmctldHost=smshost
    ```

    ??? question "Click here to learn more about the above commands."
        The default `SlurmctldHost` is set to `linux0`.  
        
        We run `perl` to search inside `/etc/slurm/slurm.conf` to find and replace any reference to `SlurmctldHost=` with `${sms_name}`.  
        
        Since the scheduler uses DNS extensively, we are inserting the DNS name for **smshost** into the `slurm.conf` file so all computers that are configured to use *Slurm* will know that the `SlurmctldHost` (Slurm control daemon) is the **smshost**.  

        `linux0` is replaced with the DNS name as defined in `${sms_name}` (the lab default is `smshost`).  

    !!! note "Tip"

        A quick test of the very first change we made to `/etc/hosts` will verify that the smshost can identify its own DNS entry.

        ```bash prefix="[root@smshost vagrant]$"
        ping smshost
        ```
        This should return output that looks like the below:
        ```{: .text .optional-language-as-class .no-copy .prevent-select}
        64 bytes from smshost (10.10.10.10): icmp_seq=1 ttl=64 time=0.026 ms
        64 bytes from smshost (10.10.10.10): icmp_seq=2 ttl=64 time=0.067 ms
        64 bytes from smshost (10.10.10.10): icmp_seq=3 ttl=64 time=0.189 ms
        ```  

    !!! danger "Important to verify the hostname order in `/etc/hosts`"
        The order of the configuration lines in `/etc/hosts` is very important.  

        The compute nodes should not resolve `smshost` to be `127.0.0.1` but rather `10.10.10.10`. If your `/etc/hosts` file has the `127.0.0.1` entry above the `10.10.10.10` entry then these must be swapped around.  

4. The default *Slurm* configuration file included with *OpenHPC* assumes dual-socket, 8 cores per socket, and 2 threads per core. However, our Virtual Cluster is designed with **1 socket**, **2 cores per socket**, and **1 thread per core**. See Table below:

    |                   | OpenHPC | **HPC Ecosystems** |
    | ----------------- | :-----: | :------------: |
    | Socket Count      | 2       | 1              |
    | Cores per Socket  | 8       | 2              |
    | Threads per Core  | 2       | 1              |

    ```bash prefix="[root@smshost vagrant]#"
    cat /etc/slurm/slurm.conf | grep Sockets
    ```

    ```{: .shell .optional-language-as-class .no-copy .prevent-select}
    OUTPUT:

    # NodeName=c[1-4] Sockets=2 CoresPerSocket=8 ThreadsPerCore=2 State=UNKNOWN 
    ```

    !!! note "Tip"
        
        For this workshop there are various approaches to changing the `slurm.conf` file:

        - Perl regular expressions _(demonstrated)_
        - *Slurm* online configuration tool
        - Manual edits
        - Replacing `slurm.conf` with `slurm.conf.lab` included in the Git repository

5. Use `perl` to change the parameters:

    ```bash prefix="[root@smshost vagrant]#"
    sudo perl -pi -e "s/NodeName=c\[1-4\]/NodeName=compute0\[0-1\]/" /etc/slurm/slurm.conf
    sudo perl -pi -e "s/Sockets=2/Sockets=1/" /etc/slurm/slurm.conf
    sudo perl -pi -e "s/CoresPerSocket=8/CoresPerSocket=2/" /etc/slurm/slurm.conf
    sudo perl -pi -e "s/ThreadsPerCore=2/ThreadsPerCore=1/" /etc/slurm/slurm.conf
    ```

    Checking the contents of `slurm.conf` should now reveal that the compute nodes are defined as **compute00** and **compute01** each with:

    - 1 Socket
    - 2 CoresPerSocket and 
    - 1 ThreadPerCore

    ```bash prefix="[root@smshost vagrant]#"
    cat /etc/slurm/slurm.conf | grep Sockets
    ```

    ```{: .shell .optional-language-as-class .no-copy .prevent-select}
    OUTPUT:

    # NodeName=compute0[0-1] Sockets=1 CoresPerSocket=2 ThreadsPerCore=1 State=UNKNOWN 
    ```

6. Replace `Nodes=c[1-4]` with `Nodes=compute0[0-1]`. This specifies that the compute nodes defined earlier are added to the default partition, `normal` . The changes can be seen in the `slurm.conf` file.

    ```bash prefix="[root@smshost vagrant]#"
    cat /etc/slurm/slurm.conf | grep Nodes
    ```
    
    ```{: .shell .optional-language-as-class .no-copy .prevent-select}
    OUTPUT:

    # PartitionName=normal Nodes=c[1-4] Default=YES MaxTime=24:00:00 State=UP Oversubscribe=EXCLUSIVE
    ```

    ```bash prefix="[root@smshost vagrant]#"
    sudo perl -pi -e "s/Nodes=c\[1-4\]/Nodes=compute0\[0-1\]/" /etc/slurm/slurm.conf
    cat /etc/slurm/slurm.conf | grep Nodes
    ```

    ```{: .shell .optional-language-as-class .no-copy .prevent-select}
    OUTPUT:

    # PartitionName=normal Nodes=compute0[0-1] Default=YES MaxTime=24:00:00 State=UP Oversubscribe=EXCLUSIVE 
    ```

    !!! note "Tip"
        The perl command for replacing the `NodeName` uses special break characters (`\`) to accommodate for the character symbols `[` and `]`.

7. In the `slurm.conf` file there are duplicate definitions for `JobCompType` and `TaskPlugin`. In order to fix this and prevent a duplicate definition error, run the following commands:

    ```bash prefix="[root@smshost vagrant]#"
    cat /etc/slurm/slurm.conf | grep 'JobCompType\|TaskPlugin' 
    ```

    ```{: .shell .optional-language-as-class .no-copy .prevent-select}
    OUTPUT:
    
    # TaskPlugin=task/affinity
    # JobCompType=jobcomp/none
    # TaskPlugin=task/affinity
    # JobCompType=jobcomp/filetxt
    ```

    ```bash prefix="[root@smshost vagrant]#"
    sudo sed -i '0,/JobCompType\=jobcomp\/none/{/JobCompType\=jobcomp\/none/d;}' /etc/slurm/slurm.conf
    sudo sed -i '0,/TaskPlugin/{/TaskPlugin/d;}' /etc/slurm/slurm.conf
    cat /etc/slurm/slurm.conf | grep 'JobCompType\|TaskPlugin' 
    ```
    
    ```{: .shell .optional-language-as-class .no-copy .prevent-select}
    OUTPUT:

    # TaskPlugin=task/affinity
    # JobCompType=jobcomp/filetxt 
    ```

## 6.2 Start/Enable Services 
---- 

1. Start/Enable *MUNGE* and *Slurm* services on the master host:

    ```bash prefix="[root@smshost vagrant]#" lines="1 4 7 8"
    sudo systemctl enable munge
    # Created symlink /etc/systemd/system/multi-user.target.wants/munge.service → /usr/lib/systemd/system/munge.service. 

    sudo systemctl enable slurmctld 
    # Created symlink /etc/systemd/system/multi-user.target.wants/slurmctld.service → /usr/lib/systemd/system/slurmctld.service. 

    sudo systemctl start munge 
    sudo systemctl start slurmctld
    ```


1. Start *Slurm* and related services on compute hosts using *pdsh* (a parallel shell tool to run a command across multiple nodes in a cluster):

    !!! Note "Registering SSH keys"
        When the **smshost** first connects to a compute node, it will add the computer to the list of known hosts on **smshost**. 

        For instance, after running the `pdsh` commands below, this will establish the first SSH connection, using SSH keys, thereby adding `compute00` and `compute01` to the list of known hosts.

        ```{: .shell .optional-language-as-class .no-copy .prevent-select}
        compute00: Warning: Permanently added 'compute00,10.10.10.100' (ECDSA) to the list of known hosts.
        compute01: Warning: Permanently added 'compute01,10.10.10.101' (ECDSA) to the list of known hosts.
        ```

    First, we will instruct the compute nodes to start the `chronyd` service, and then verify that the service is started and `running` on both compute nodes.  

    ```bash prefix="[root@smshost vagrant]#"
    sudo pdsh -w ${compute_prefix}[00-01] "systemctl start chronyd" 
    sudo pdsh -w ${compute_prefix}[00-01] "systemctl status chronyd" | grep running
    ```

    ```{: .shell .optional-language-as-class .no-copy .prevent-select}
    OUTPUT:

    # compute00:    Active: active (running) since Wed 2023-03-29 17:36:10 UTC; 3h 4min ago
    # compute01:    Active: active (running) since Wed 2023-03-29 20:40:45 UTC; 18s ago
    ```

    ??? question "Click here to learn more about `pdsh`."
        `pdsh` is a parallel Shell tool that allows a single command to be sent to multiple targets. Whenever you call `pdsh` you can interpret this to be a sequence of the same command being issued individually to each recipient that is specified in the target range.  

        *** 

        `compute_prefix` represents the standard prefix you have allocated for each compute node in your cluster, and `[00-01]` says that you are talking to the nodes in the range `00` to `01`.  

        ***

        The oder that you receive replies back is unpredictable, but every response will be preceded by the computer name (e.g. `compute00` or `compute01`) so pay careful attention to the prefix!  

    !!! note "Doing things more methodically without `pdsh`."
        If you experience difficulty with `pdsh` or want to take a more methodical step-by-step approach, you can substitute all the `pdsh` commands for a series of commands that are issued once you have manually `ssh`'ed to each compute node.  

    Next, we will instruct the compute nodes to start the MUNGE service, and then verify that the service is started and `running` on both compute nodes.  

    ```bash prefix="[root@smshost vagrant]#"
    sudo pdsh -w ${compute_prefix}[00-01] "systemctl start munge"
    sudo pdsh -w ${compute_prefix}[00-01] "systemctl status munge" | grep running
    ```

    ```{: .shell .optional-language-as-class .no-copy .prevent-select}
    OUTPUT:

    # compute01:    Active: active (running) since Wed 2023-03-29 20:40:45 UTC; 50s ago
    # compute00:    Active: active (running) since Wed 2023-03-29 17:36:10 UTC; 3h 5min ago
    ```

    *** 

    Finally, we will instruct the compute nodes to start the *Slurmd* client service, and then verify that the service is started and `running` on both compute nodes.  


    ```bash prefix="[root@smshost vagrant]#"
    sudo pdsh -w ${compute_prefix}[00-01] "systemctl start slurmd" 
    sudo pdsh -w ${compute_prefix}[00-01] "systemctl status slurmd" | grep running 
    ```

    ```{: .shell .optional-language-as-class .no-copy .prevent-select}
    OUTPUT:

    # compute00:    Active: active (running) since Thu 2023-03-30 06:56:14 UTC; 53min ago
    # compute01:    Active: active (running) since Thu 2023-03-30 07:46:15 UTC; 3min 26s ago
    ```

    ??? danger "Click here if you encounter a `libhwloc.so.15 missing` error." 

        If you encounter a problem starting `slurmd` service on the compute nodes, you can verify the reason by invoking `systemctl status slurmd`. 
        
        If the error relates to `libhwloc.so.15: cannot open shared object file: No such file or directory` then this can be solved with the following steps (NOTE: less common errors are potentially addressed in the [virtual lab FAQ section](9_faq.md));

        ```bash
        sudo dnf --installroot=$CHROOT install hwloc-libs
        ```

        When working with *Warewulf*, any updates to a diskless image's `chroot` must be encapsulated and compressed into a *Virtual Node File System (VNFS)* image that *Warewulf* can provision. As mentioned before, changes to `chroot` is akin to modifying source code, and *VNFS* is the compiled binary from the source code.

        ```bash
        sudo wwvnfs --chroot $CHROOT
        ```

        Once the image is recompiled and added to the datastore:

        1. reboot the compute node VM to have the compute node boot from the updated compute image.  

            !!! note "Tip - using SSH to reach a compute node."
                You can SSH to a compute node as `root` user. Pay careful attention to the following commands and notice which host the commands are being issued from.  


        1. SSH to the compute node and verify the status of the `slurmd` service.  

            ```bash prefix="[root@compute01 ~]#"
            sudo systemctl status slurmd
            ```
            ```{: .shell .optional-language-as-class .no-copy .prevent-select}
            OUTPUT:

            # ● slurmd.service - Slurm node daemon
            # Loaded: loaded (/usr/lib/systemd/system/slurmd.service; disabled; vendor preset: disabled)
            # Active: active (running) since Sat 2023-08-26 11:16:48 UTC; 1min 10s ago
            # Main PID: 1113 (slurmd)
            # Tasks: 2
            # Memory: 1.4M
            # CGroup: /system.slice/slurmd.service
            #         └─1113 /usr/sbin/slurmd -D -s --conf-server 10.10.10.10
            ```    


    ***
        
1. Ensure the *Slurm* service started up successfully on the **smshost**:

    ```bash prefix="[root@smshost vagrant]#"
    systemctl status slurmctld
    ```    


1. Verify the *Slurm* subsystem is working correctly by inspecting `sinfo`:

    ```bash prefix="[root@smshost ~]#"
    sudo sinfo 
    ```
    If everything is configured correctly, `sinfo` should return something that resembles the following output:

    ```{: .shell .optional-language-as-class .no-copy .prevent-select}
    PARTITION AVAIL  TIMELIMIT  NODES  STATE NODELIST
    normal*      up 1-00:00:00      2   idle compute[00-01]
    ```

    If the `STATE` is `idle` then the compute nodes are talking correctly with the *Slurm* manager.

    ??? question "Click here to learn more about the `sinfo` output."
        The `STATE` of the available compute nodes should not have a `*` next to them.  
        
        If any node has a `*` in the `STATE` column, then that means the node is *unreachable*.  
        
        An *unreachable* node can be so for many reasons - please consult [the FAQ](9_faq.md) for assistance to debug the possible problems.  

        ***  

        The only desirable `STATE` for your cluster right now is `idle` which means the nodes are reachable and ready to perform a task.  

## 6.3 Prepare for Test Job
----
 
 With the resource manager successfully enabled, users are able to submit jobs for processing. We will add a `test` user on the **smshost** for running an example job.

```bash prefix="[root@smshost vagrant]#"
sudo useradd -m test 
```

!!! quote "Note"

    In chapter 4 we registered credenital files with *Warewulf* (e.g. `passwd`, `group` and `shadow`) so that these files are propogated during compute node imaging. *Warewulf* installs a utility on the compute nodes to automatically synchronize known files from the provisioning server at five minute intervals but with a new `test` user just recently added these files have become outdated for the time being, until the next file synchronisation.

To update the *Warewulf* database to incorporate the additions, run the following resync process:

```bash prefix="[root@smshost vagrant]#"
sudo wwsh file resync passwd shadow group 
```

??? note "Click here to learn more about how to `watch` this procedure."

    As part of the learning exercise, you can run a separate session (using `tmux`, for instance which is pre-installed on the virtual lab image) to watch the users files on the compute node, and observe when the file resync is completed.  
    
    From a compute node:
    ```bash
    watch 'cat /etc/passwd | grep test'
    ```

??? question "Click here to learn how to force a *Warewulf* file transfer without waiting."

    After re-syncing (which notifes *Warewulf* of file modifications made on the **smshost**), it will take approximately 5 minutes for the changes to propagate. However, you can also manually force the results with `/warewulf/bin/wwgetfiles` as follows:

    ```bash prefix="[root@smshost vagrant]#"
    sudo pdsh -w ${compute_prefix}[00-01] /warewulf/bin/wwgetfiles 
    ```

??? example "Click here to recap this chapter."

    In this chapter you have successfully configured and started up *Slurm* - the HPC cluster's resource manager, which will manage all jobs submitted to the cluster.  
    - You configured the `SlurmctldHost` to match the DNS name of your **smshost**.  
    - You updated the `slurm.conf` file with revised compute node specifications for `Sockets`, `CoresPerSocket`, and `ThreadsPerCore`.  
    - You started the MUNGE and Slurm services on the **smshost** and the compute nodes (note that the **smshost** runs the `Slurmctld` *Slurm* Controller Daemon, while the compute nodes run `slurmd` - the *Slurm* Daemon).  
    - You ensured that `chronyd` was running on the compute nodes, to keep time synchronised across the **smshost** and compute nodes.  
    - You verified the scheduler state by running `sinfo` and verifying the compute nodes are in an `idle` state (ready to accept jobs).  
    - You added a `test` user to **smshost** which was then propagated to the compute nodes through *Warewulf*'s file synchronisation settings.  

!!! success "Congratulations"

    **You have reached the end of Chapter 6 - Well done!**<br>




***

## Bug report
??? bug "Click here if you wish to report a bug."

    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSf-msvHOP9HpU-q3P1PpyAiT17CfFRGKxZQ6wxODnBUpzut2g/viewform?embedded=true" width="640" height="610" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>

## Provide feedback  
??? abstract "Click here if you wish to provide us feedback on this chapter."

    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLScfXUQc06hHEjdqqAdXf74-BCo9_bFUdud974wdJnEUxgnTxg/viewform?embedded=true" width="640" height="997" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>