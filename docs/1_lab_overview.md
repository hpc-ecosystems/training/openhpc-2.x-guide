This section will introduce the **guide** and the **virtual lab environment**. 

- The initial preparation for the lab involves **downloading and installing** the appropriate **software and configuration files** needed to complete the guide [(Chapter 2)](2_virtual_lab_setup.md).

- Following this you will **set up the System Management Server (SMS)** host [(Chapter 3)](3_management_node_setup.md). The **smshost** is responsible for managing the virtual cluster. 

- Next you will **prepare the compute node image** [(Chapter 4)](4_compute_node_preparation.md) such that the compute node virtual machines can be provisioned by *Warewulf* [(Chapter 5)](5_cluster_provisioning.md).

- The remainder of the guide involves further **preparing the virtual cluster to run HPC applications** and **running test jobs** in the virtual cluster environment using *Slurm*. 
 

!!! quote "Note"

    Anticipated time to complete this chapter: TBC from user feedback. 

!!! danger "Important"

    This guide uses *VirtualBox* for the **virtual lab environment**.

    *Vagrant* is used to manage the VirtualBox **Virtual Machine (VM) configuration**.

    Although it is technically feasible to run a VM as part of a physically deployed HPC system, YOU DO **NOT** NEED VIRTUALBOX OR VAGRANT FOR THE FINAL HPC DEPLOYMENT.

    *VirtualBox* and *Vagrant* are used within this virtual lab to **simulate a virtual cluster** and **simplify the VM deployment process**, respectively.



## 1.1 VM Setup using Vagrant
---
We will use *Vagrant* to define and manage virtual machines in the *VirtualBox* environment. This approach ensures a straightforward and consistent VM deployment for all users. There exists a *Vagrant* definition for both the **smshost** and the **compute hosts**. These host definitions are captured in a file called a `Vagrantfile`.  

??? question "Click here for more information about the way Vagrant uses `Vagrantfile`"
    *Vagrant* acts as a broker between the user and the Hypervisor. We are using *VirtualBox* in this virtual lab, but *Vagrant* can communicate with many Hypervisors while reducing the complexity for the user by requiring a single  language to communicate with *Vagrant*.
    
    Information defined in `Vagrantfile` is used by *Vagrant* to communicate with *VirtualBox* to modify parameters and specifications for the lab VMs.  
    
    As you progress through the virtual lab, you will observe that *Vagrant* performs the same tasks that a user can accomplish through the *VirtualBox* interface - the main advantage of using *Vagrant* is to ensure consistency, repeatability, and scalability by automating the process and keeping a single source of truth.  

    For example, it is much simpler to read the MAC addresses of all the VMs from the `Vagrantfile` than it would be to manually cycle through all the VMs in the VirtualBox interface to find this information.

### 1.1.1 **Smshost**

The **smshost** VM is instantiated with a base Rocky Linux 8 operating system image. This is the standard software environment that is used in the official OpenHPC install recipe.  

For the virtual lab the image is sourced from the *Bento* provider through *Vagrant Cloud*.

The **smshost** is defined in our `Vagrantfile` as follows:

- **Rocky Linux 8 Image**
- **1GB RAM**
- **2 virtual CPUs**
- **Hostname: *smshost***
- **External virtual network interface**: for *internet connectivity* and *ssh* access
- **Internal virtual network interface**: for communication over the internal cluster network (**hpcnet**)


The custom **software additions** to the base VM image include: 

- Extra **packages**: 
    - Such as `tmux` and `vim` 
- The environment **configuration file** `input.local.lab` 

??? question "Click here to understand why we've added these packages"

    **tmux**  
    `tmux` is a terminal multiplexor that allows you to resume a shell session even after disconnecting. It does a lot more than that, and a really useful feature for the learning experience is the option to create multiple panes or windows in the shell. If you want to maximise your learning experience, you can open two panes on the same shell session so you can `watch` a file that you are about to apply changes to, and then see what happens after you issue the command.  

    **vim**  
    `vim` is a quality-of-life improvement over the included `vi` but we aren't going to get into an argument over which terminal editor to use!  

    **input.local.lab**  
    `input.local.lab` is a trimmed down `input.local` parameter definition file that we use for the virtual lab. It contains only the information that is necessary to deploy the virtual cluster.  
    
    It has a lot of functional similarities to the `Vagrantfile` in the sense that *Vagrant* uses the `Vagrantfile` to define parameters for the Provider, and *OpenHPC* uses the `input.local.lab` file to define parameters for the OS configuration.  
    
    If you inspect `input.local.lab` you will see a series of environment variable names and their associated values. If you ever need to run cluster-wide changes with OpenHPC, you can modify a single source of information (`input.local` in a production deployment of OpenHPC) and reconfigure with ease. The alternative approach would be to try to remember the path to a specific compute node OS image, or determining the MAC address for `compute23` some other way - on large production systems of hundreds or thousands of nodes it is easy to see why keeping all resource information in a single location can be easier to manage, and also easier to secure with backups.

### 1.1.2 Compute hosts

The `Vagrantfile` definition for the **compute hosts** differs to that of the **smshost**. This is because the OS image for these hosts is provided by *Warewulf* via **PXE** on boot. Since the compute nodes are 'stateless', their images are configured and stored on the **smshost** and loaded into the compute node's memory on boot. This will be explained further and demonstrated in the chapters that follow. 

*Vagrant* uses 'empty' `.box` file (i.e. no OS image) as a base and upon which the hardware configuration for the compute node can be built using the `Vagrantfile`. This file, `compute-node.box`, is stored locally in the lab directory pulled from the [GitLab repo.](https://gitlab.com/hpc-ecosystems/training/openhpc-2.x-virtual-lab)

The **compute nodes** have the following *Vagrant* definition:

- **No OS image**
- **3GB RAM**
- **2 virtual CPUs**
- **Hostname: *compute[00-01]***
- **Internal virtual network interface**: for communication over the internal cluster network (**hpcnet**)

??? question "Click here to understand why we use stateless compute nodes and what a Vagrant `.box` file does."

    **Stateless Compute Nodes?**
    
    Historically, some HPC clusters came in a 'stateless' compute node configuration (i.e. no local operating system installed on the compute nodes) while others sometimes came with a 'stateful' compute node configuration (i.e. a local operating system installed on a compute node's local hard-drive). There were arguments in favour of both approaches (boot times, RAM consumption, flexibility, etc.) but over time the tradition has tended to be 'stateless wins'. The flexibility of customising the compute node operating system environment by pushing updated boot images over the network seems to be winning favour more so than the boot time saved by a 'stateful' configuration. Insofar as OpenHPC is concerned, the 'stateless' approach has been adopted wholesale since OpenHPC 2.x.

    **Vagrant boxes?**  

    A `.box` file contains all the information for a provider (a Hypervisor) to launch a Vagrant machine - these include: 
    - the VM image  
    - metadata  
    - the `Vagrantfile`

    [Click here for more information on the Vagrant `.box` file.](https://developer.hashicorp.com/vagrant/docs/boxes/format)

## 1.2 OpenHPC
---

The *OpenHPC* guide relies on `${variable}` substitution in the command line. Although it is possible to manually substitute these variables at each step, it is **strongly recommended** to use the **`input.local.lab`** file to define IP adresses, hardware MAC adresses, and so on. 

!!! note "Tip"

    The default values in `input.local.lab` have been thoroughly tested for the virtual lab deployment as outlined in the guide. It is strongly recommended that you do not modify them.

??? question "Click here to learn more about the specific design decisions in `input.local.lab`"

    `c_ip` IPv4 Addresses:  
    We have chosen the range `10.10.10.0/24` for several reasons:  

    - this is an easy-to-remember private IP range  
    - it's quite easy to type!  
    - a legacy system (Ranger) had iLOM which is a 'lights out' management network range, and the HPC Ecosystems Project has stuck with the convention of production being `10.10.10.0/24` with the management (now 'iLO' and 'iDRAC') being `10.10.11.0/24` since the `11` resembles the 'iL' part of 'iLOM'. Kinda sad, right?  

    ***

    `sms_ip=10.10.10.10`:  
    The *smshost* IPv4 is `10.10.10.10` because we want to have all compute nodes numbered from `00` upwards having an easy to map IPv4 address.  
    
    In our virtual cluster, that means:  
    - `compute`**00** will be `10.10.10.1`**00**, and  
    - `compute`**01** will be `10.10.10.1`**01**  
    - and so forth.  

    This should make it fairly intuitive to map a specific compute node to an IP address and vice versa.  

    ***

    You are free to change this convention, but it's something that's stuck with our community and subsequently for the virtual lab.  

    That's it really - there's no profound technical reason for this - it's just legacy convenience!

## 1.3 Cluster Layout
---

This virtual lab involves configuring three lightweight virtual machines that are used to create a virtual cluster using the *VirtualBox* hypervisor. The layout of the virtual cluster that will be deployed is depicted in [Figure 1.](#figure1)

!!! danger "Important"

    Make sure that you understand the overview, because you will be adding your specifications to the figure.

!!! quote "Note"

    Whereas the original OpenHPC recipe expects **four** compute nodes, we will work with **two**. 

<a name="figure1"></a>
[<img id="fig1" alt="Figure 1: Overview of the Virtual Cluster layout" src="../_media/figure1.png" />]()
<center>Figure 1: Overview of the Virtual Cluster layout</center>

??? question "Click here to understand the layout of the virtual cluster."

    The virtual cluster depicted in [Figure 1.](#figure1) consists of four 'components':

    1. The **smshost** - the server that performs the role of management, login, and storage. In the figure, `eth0` is a public-facing interface to allow remote logins to the cluster, while `eth1` is an internal-facing interface that communicates with the cluster. 

    1. The **hpcnet** internal HPC network - this is an internal private network that is exclusively for HPC-related traffic and communications. It is entirely separate from any other production network. As is evident in the diagram, only the compute nodes and the **smshost** are connected to this network. A common misconception is that an HPC system needs wholesale network exceptions and firewall rules etc. to be allowed in a production environment. In reality, as the diagram shows, it is an entirely private and separate network and it is only accessible via (in this case) the *login node*. 

    1. The **compute nodes** - the workhorses of the HPC cluster. These are typically 'stateless' systems that pull their OS image via a provisioning network connected to the *management* server (in this case, **smshost** via the private internal network **hpcnet**).

    1. The **internet / public / WAN** - this is the route that users will take to connect to the cluster. In reality, users will use a remote access protocol (by and large, this will be `ssh` but with recent software tools being developed by HPC experts, it's not exclusively `ssh`!) to connect to a *login node* to authenticate onto the cluster. All requests to the actual *compute nodes* will be relayed through the software on the *management node* and *scheduled* to run on the cluster when resources are available. The diagram shows that users talk to `eth0` (the public-facing interface of **smshost**) which then talks to the cluster through `eth1` (the internal private `hpcnet`).  

    As mentioned already, the learning cluster that we are using in the virtual lab is a **three node** virtual cluster - a *management node* and two *compute nodes*. 

??? example "Click here to recap the chapter."

    You have been equipped with all the basic knowledge that you need to continue with the virtual lab. In the next chapter we will start installing the software to your local machine that is required to prepare the virtual lab. [On to chapter 2!](2_virtual_lab_setup.md)



***

## Bug report
??? bug "Click here if you wish to report a bug."

    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSf-msvHOP9HpU-q3P1PpyAiT17CfFRGKxZQ6wxODnBUpzut2g/viewform?embedded=true" width="640" height="610" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>

## Provide feedback  
??? abstract "Click here if you wish to provide us feedback on this chapter."

    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLScfXUQc06hHEjdqqAdXf74-BCo9_bFUdud974wdJnEUxgnTxg/viewform?embedded=true" width="640" height="997" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>