# OpenHPC 2.x Guide 

**Welcome to the HPC Ecosystems OpenHPC 2.x Training Guide!** We are excited to have you along for the journey.

In the sections to follow you will find a comprehensive *HOW-TO* guide to set up **OpenHPC 2.x** on a virtual cluster. 

This guide was initially developed to ensure HPC software stack parity across partner sites in the [**HPC Ecosystems Community**](https://ecosystems.nicis.ac.za/p/our-community-sites.html) but the interest in our work has grown well beyond our HPC community and this is now our contribution towards training aspiring HPC System Administrators across the world! 

Anyone wishing to learn more about the deployment of the OpenHPC 2.x software stack is likely to find value in this content. 

[Let’s go ahead and jump right in!](0_introduction.md)
