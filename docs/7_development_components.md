At this stage, you have:  

1. successfully installed and configured the master host ([**chapter 3**](3_management_node_setup.md)),  
1. assembled and customised the compute image ([**chapter 4**](4_compute_node_preparation.md)),  
1. and provisioned several compute hosts from baremetal ([**chapter 5**](5_cluster_provisioning.md)).  
1. Additionally, you have also configured your resource manager to schedule jobs on your virtual cluster ([**chapter 6**](6_resource_management.md)). 

You essentially have a functional virtual computer cluster!

All that's left to do for the real HPC experience is to add additional packages, compilers, tools and libraries to support a flexible HPC development environment. 

The following subsections will guide you through the installation procedures for installing some popular tools falling under this category of additional software. 

!!! quote "Note"

    Anticipated time to complete this chapter: TBC from user feedback.


## 7.1 OpenHPC Development Components
--------

To aid in general development efforts, *OpenHPC* provides recent versions of several development tools packaged for/with the *OpenHPC* environment (appended with `-ohpc`):

- `autotools` - [GNU Autotools](https://opensource.com/article/19/7/introduction-gnu-autotools#:~:text=For%20developers%2C%20Autotools%20is%20a%20quick%20and%20easy,can%20easily%20prepare%20a%20project%20built%20with%20Autotools) manages and packages source code to allow users to compile and install software.
  
- `EasyBuild` - [EasyBuild](https://docs.easybuild.io/what-is-easybuild/) is a software build and installation framework.
- `hwloc` - [Portable Hardware Locality](https://www.open-mpi.org/projects/hwloc/) provides a portable abstraction of hierarchical topology of modern architectures.
- `spack` - [Spack](https://spack.io/) is a package manager for supercomputers, Linux, and macOS.
- `valgrind` - [Valgrind](https://valgrind.org/) is an instrumentation framework for building dynamic analysis tools - popular among them is memory management and threading debugging.

!!! Note "Taking things further"
    To allow for further exploration of the *OpenHPC* virtual cluster environment, we will install these development tools. Even though this virtual lab will reach a conclusion, you are encouraged to take your journey deeper, and these tools should provide a good starting platform whether you want to explore System Administration or scientific computing within an HPC environment.

```bash prefix="[root@smshost vagrant]#"
sudo dnf -y install ohpc-autotools # 11M Installed Size
```

```{: .shell .optional-language-as-class .no-copy .prevent-select}
OUTPUT:

# Installed:
#   autoconf-ohpc-2.69-3.6.ohpc.2.0.x86_64          automake-ohpc-1.16.1-1.8.ohpc.2.0.x86_64
#   libtool-ohpc-2.4.6-1.5.ohpc.2.0.x86_64          ohpc-autotools-2.6.1-5.1.ohpc.2.6.1.x86_64
```

```bash prefix="[root@smshost vagrant]#"
sudo dnf -y install EasyBuild-ohpc # 13M
```

```{: .shell .optional-language-as-class .no-copy .prevent-select}
OUTPUT:

# Installed:
#   EasyBuild-ohpc-4.6.2-6.1.ohpc.2.6.x86_64    python3-pip-9.0.3-22.el8.rocky.0.noarch
#   python3-setuptools-39.2.0-6.el8.noarch      python36-3.6.8-38.module+el8.5.0+671+195e4563.x86_64
```

```bash prefix="[root@smshost vagrant]#"
sudo dnf -y install hwloc-ohpc # 17M Installed size
```

```{: .shell .optional-language-as-class .no-copy .prevent-select}
OUTPUT:

# Installed:
#   cairo-1.15.12-6.el8.x86_64                           dejavu-fonts-common-2.35-7.el8.noarch
#   dejavu-sans-fonts-2.35-7.el8.noarch                  fontconfig-2.13.1-4.el8.x86_64
#   fontpackages-filesystem-1.44-22.el8.noarch           hwloc-ohpc-2.7.0-3.9.ohpc.2.6.x86_64
#   libX11-1.6.8-5.el8.x86_64                            libX11-common-1.6.8-5.el8.noarch
#   libXau-1.0.9-3.el8.x86_64                            libXext-1.3.4-1.el8.x86_64
#   libXrender-0.9.10-7.el8.x86_64                       libxcb-1.13.1-1.el8.x86_64
#   pixman-0.38.4-2.el8.x86_64
```

```bash prefix="[root@smshost vagrant]#"
sudo dnf -y install spack-ohpc # 91M
```

```{: .shell .optional-language-as-class .no-copy .prevent-select}
OUTPUT:

# Upgraded:
#   libstdc++-8.5.0-16.el8_7.x86_64                libstdc++-devel-8.5.0-16.el8_7.x86_64
# Installed:
#   gcc-c++-8.5.0-16.el8_7.x86_64
#   libserf-1.3.9-9.module+el8.7.0+1065+42200b2e.x86_64
#   mercurial-4.8.2-1.module+el8.3.0+219+18f2b388.x86_64
#   python2-2.7.18-11.module+el8.7.0+1062+663ba31c.rocky.0.2.x86_64
#   python2-libs-2.7.18-11.module+el8.7.0+1062+663ba31c.rocky.0.2.x86_64
#   python2-pip-9.0.3-19.module+el8.6.0+793+57002515.noarch
#   python2-pip-wheel-9.0.3-19.module+el8.6.0+793+57002515.noarch
#   python2-setuptools-39.0.1-13.module+el8.4.0+403+9ae17a31.noarch
#   python2-setuptools-wheel-39.0.1-13.module+el8.4.0+403+9ae17a31.noarch
#   python3-mock-2.0.0-11.el8.noarch
#   spack-ohpc-0.18.1-3.5.ohpc.2.6.x86_64
#   subversion-1.10.2-5.module+el8.7.0+1065+42200b2e.x86_64
#   subversion-libs-1.10.2-5.module+el8.7.0+1065+42200b2e.x86_64
#   utf8proc-2.6.1-3.module+el8.7.0+1065+42200b2e.x86_64
#   zstd-1.4.4-1.el8.x86_64
```

```bash prefix="[root@smshost vagrant]#"
sudo dnf -y install valgrind-ohpc # 14M Download 56M Installed
```

```{: .shell .optional-language-as-class .no-copy .prevent-select}
OUTPUT:

# Installed:
#   valgrind-ohpc-3.19.0-4.1.ohpc.2.6.x86_64
```

## 7.2 Compilers
--------

*OpenHPC* packages the *GNU* compiler toolchain integrated with the underlying *Lmod* modules system in a hierarchical fashion. 

```bash prefix="[root@smshost vagrant]#"
sudo dnf -y install gnu9-compilers-ohpc # 47M / 178M Installed Size
```

```{: .shell .optional-language-as-class .no-copy .prevent-select}
OUTPUT:

# Installed:
#   gnu9-compilers-ohpc-9.4.0-4.1.ohpc.2.4.x86_64
```

??? question "Click here to learn more about Lmod."
    Lmod is a Lua based module system that handles `MODULEPATH` hierarchies and provides the same function as the `module` command, but with a slightly different (but in many ways overlapping) syntax.  

    [For more information about Lmod, click here.](https://lmod.readthedocs.io/en/latest/index.html)

## 7.3 MPI Stacks
--------

For MPI development and runtime support, *OpenHPC* provides pre-packaged builds for a variety of MPI families and transport layers. These are compatible with both ethernet and high-speed fabrics. 

We will install *Open MPI v4.0* and *MPICH* (High-Performance Portable MPI), below:

```bash prefix="[root@smshost vagrant]#"
sudo dnf -y install openmpi4-gnu9-ohpc mpich-ofi-gnu9-ohpc # 13M
```

```{: .shell .optional-language-as-class .no-copy .prevent-select}
OUTPUT:

# Installed:
#   libfabric-ohpc-1.13.0-3.2.ohpc.2.4.x86_64         libpsm2-11.2.206-1.el8.x86_64
#   librdmacm-41.0-1.el8.x86_64                       mpich-ofi-gnu9-ohpc-3.4.2-3.1.ohpc.2.4.x86_64
#   openmpi4-gnu9-ohpc-4.1.1-10.1.ohpc.2.4.x86_64     prun-ohpc-2.2-2.1.ohpc.2.4.noarch
#   ucx-ib-ohpc-1.11.2-3.3.ohpc.2.6.x86_64            ucx-ohpc-1.11.2-3.3.ohpc.2.6.x86_64
```


## 7.4 Performance Tools
--------

*OpenHPC* provides a variety of open-source tools to aid in application performance analysis, including:

- [perf](https://en.wikipedia.org/wiki/Perf_%28Linux%29) - a performance analysing tool in Linux capable of statistical profiling of the entire system.
- [TAU](https://hpc-wiki.info/hpc/Tau) - *Tuning and Analysis Utilities* is a toolkit for the performance analysis of parallel programs by profiling and tracing.
- [Scalasca](https://www.scalasca.org/) - a software tool that supports the performance optimisation of parallel programs.
- [Score-P](https://hpc-wiki.info/hpc/Score-P) - a performance measurement infrastructure for profiling, event tracing and online analysis of HPC applications.
- [Dimemas](https://tools.bsc.es/dimemas) - a performance analysis tool for message-passing programs. 

```bash prefix="[root@smshost vagrant]#"
sudo dnf -y install ohpc-gnu9-perf-tools # 474MB installed to 2.3GB
```

## 7.5 Set Up Default Environment
----

System users often find it convenient to have a default development environment in place so that compilation can be performed directly for parallel programs requiring MPI. This setup can be conveniently enabled via modules. 

The *OpenHPC* modules environment is pre-configured to load an `ohpc` module on login. 

The following package install provides a default environment that enables `autotools`, the GNU compiler toolchain, and the *OpenMPI* stack.

```bash prefix="[root@smshost vagrant]#"
sudo dnf -y install lmod-defaults-gnu9-openmpi4-ohpc
```

??? example "Click here for a recap of this chapter."
    In this chapter you successfully installed software to support a flexible development environment:  

    - You installed OpenHPC Development Components, including GNU Autotools, EasyBuild, Spack, and Valgrind.  
    - You installed the GNU compiler toolchain and the *Lmod* modules system.  
    - You installed some MPI development families: *Open MPI v4.0* and *MPICH*.  
    - You installed some performance tools to aid performance analysis, including `perf`.  
    - You set up the default development environment that enables `autotools`, GNU compiler toolchain, and the *OpenMPI* stack.  

!!! success "Congratulations"

    **You have reached the end of Chapter 7**

    In the [next chapter](8_running_jobs.md) you will run your first test job on your virtual cluster.



***

## Bug report
??? bug "Click here if you wish to report a bug."

    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSf-msvHOP9HpU-q3P1PpyAiT17CfFRGKxZQ6wxODnBUpzut2g/viewform?embedded=true" width="640" height="610" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>

## Provide feedback  
??? abstract "Click here if you wish to provide us feedback on this chapter."

    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLScfXUQc06hHEjdqqAdXf74-BCo9_bFUdud974wdJnEUxgnTxg/viewform?embedded=true" width="640" height="997" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>