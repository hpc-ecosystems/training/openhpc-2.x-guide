import logging

from bs4 import BeautifulSoup
from mkdocs.structure.pages import Page


def inject_link(html: str, href: str,
                page: Page, logger: logging) -> str:

    logger.info(f'(hook on inject_link: {page.title})')
    soup = BeautifulSoup(html, 'html.parser')
    footer = soup.select('.md-copyright')
    nav = soup.find(class_='md-header-nav')
    if footer and footer[0]:
        container = footer[0]

        container.append(' | ')
        a = soup.new_tag('a', href=href, title='PDF',
                         download=None, **{'class': 'link--pdf-download'})
        a.append('Download PDF')

        container.append(a)

        return str(soup)

    return html
